# Catálogo de Livros

Este projeto é um desafio técnico proposto pela Zygo em seu processo seletivo para vaga de backend, no qual é necessário construir um sistema simples onde usuários podem acessar e buscar por livros.

## Como utilizar

Para rodar a aplicação é necessário ter instalado [docker](https://docs.docker.com/get-docker/) e [docker-compose](https://docs.docker.com/compose/install/)

Inicie a aplicação com o comando:

```bash
docker-compose up
```

Acesse o site através do endereço:

```bash
http://localhost:3000
```

Para rodar os testes com rspec:

```bash
docker-compose exec rails bundle exec rspec 
```

## Escolhas

Por ser um sistema mais simples adicionei apenas duas tabelas, uma para armazenar as informações dos livros e outra para os usuários `admin`.

* Docker
  - Para facilitar o desenvolvimento e a execução em outras máquinas
* Devise
  - A gem já constrói tudo que é necessário para autenticar usuários
* PG_search
  - Para que a busca no banco não seja um possível alvo de `sql injection`
* Stimulux Reflex
  - Aproveitei a oportunidade para usar o Stimulux, que já estava querendo testar há um tempo 
* Bootstrap
  - Framework mais simpático para quem, como eu, não tem muita experiência com javascript
* Features branch
  - Para facilitar a dição de novas features e alguns testes de implementação sem afetar o código que já está funcionando

## Próximos Passos

* Separar por categoria:
  - Adicionar um `enumerable` no attributo e delimitar algumas categorias nas quais os livros poderiam ser cadastrados, assim podendo filtrar também por esse campo mais facilmente
* Guardar imagem active storage:
  - Além da opção de armazenar uma url da imagem do livro, ter a opção de salvar um arquivo de imagem no banco com `ActiveStorage`.
* Usar o Redis para guardar o cache de sessão:
  - Adicionar um container para o Redis e armazenar sessão, com tempo definido, em memória para melhorar o desempenho da aplicação
* Capybara:
  - Testes de feature 
* I18n:
  - Internacionalizar a aplicação, principalmente as mensagens do Devise que não foram traduzidas
* Mover os filtros para um `Service Object`:
  - Com o possível aumento dos filtros, por categoria por exemplo, seria interessante mover toda a lógica dos filtros para outra classe para não deixar o `controller` com código demais
* Remover o cadastrar do formulário de login
  - Para que somente seja possível adicionar apenas alguns usuários que vão administrar os livros, modificar para que aparece apenas para usuários que tenham feito login e queiram adicionar outro usuário