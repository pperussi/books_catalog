FactoryBot.create(:book, image_url: 'https://m.media-amazon.com/images/I/91rtArfzScL.jpg')
FactoryBot.create(:book, image_url: 'https://images-na.ssl-images-amazon.com/images/I/81SWBRKfExL.jpg')
FactoryBot.create(:book, image_url: 'https://m.media-amazon.com/images/I/51XULadddlL.jpg')

5.times do
  FactoryBot.create(:book)
end

FactoryBot.create(:book, image_url: 'https://images-na.ssl-images-amazon.com/images/I/613Ywe4F3vL.jpg')
FactoryBot.create(:book, image_url: 'https://images-na.ssl-images-amazon.com/images/I/8143nsPug3L.jpg')
FactoryBot.create(:book, image_url: 'https://www.lpm.com.br/livros/imagens/o_cortico_capa_nova_9788525409171_hd.jpg')

2.times do
  FactoryBot.create(:book)
end

FactoryBot.create(:book, image_url: 'https://images-na.ssl-images-amazon.com/images/I/51Bcp998vTL._SX353_BO1,204,203,200_.jpg')
FactoryBot.create(:book, image_url: 'https://m.media-amazon.com/images/I/311wxEmd-dL.jpg')

Admin.create(email: 'admin@admin.com', password: 'adminadmin')