class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :title, null: false, index: { unique: true }
      t.text :description, null: false
      t.string :category, null: false
      t.integer :pages, null: false
      t.string :author, null: false
      t.string :image_url

      t.timestamps
    end
  end
end
