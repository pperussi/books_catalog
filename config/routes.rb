# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :admins
  root to: "books#index"
  resources :books
  resources :about, only: :index
end
