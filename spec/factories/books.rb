FactoryBot.define do
  factory :book do
    title { Faker::Book.unique.title }
    description { Faker::Lorem.paragraph }
    category { Faker::Book.genre }
    pages { rand(40...999) }
    author { Faker::Book.author }
    image_url { nil }

    trait :with_url do
      image_url { 'https://images-na.ssl-images-amazon.com/images/G/32/br-books/2021/June/Optimus_Indicacoes/440x550.jpg' }
    end
  end
end