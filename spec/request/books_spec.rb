# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/books', type: :request do
  let(:admin) { create(:admin) }

  let(:login) do
    post admin_session_path, params: {
      admin: {
        email: admin.email, password: admin.password
      }
    }
  end

  describe 'GET #index' do
    it 'requests list of all books' do
      book_one = create(:book)
      book_two = create(:book)
      book_three = create(:book)

      get('/books.json')

      expect(response.body).to include(book_one.title, book_two.title, book_three.title)
    end
  end

  describe 'GET #show' do
    it 'request one book details' do
      book = create(:book)

      get book_url(book.id)

      expect(response.body).to include(book.title)
    end
  end

  describe 'POST #new' do
    subject(:new_book) do
      post books_url, params: params
    end

    let(:params) do {
        book: { 
          title: Faker::Book.unique.title,
          description: Faker::Lorem.paragraph,
          pages: 100,
          category: Faker::Book.genre,
          author: Faker::Book.author
        }
      }
    end

    it 'creates a new book' do
      login

      expect{new_book}.to change{Book.count}.by(1)
    end
  end

  describe 'PUT #update' do
    before do
      create(:book)
    end

    subject(:edit_book) do
      put book_url(Book.last.id), params: params
    end

    let(:params) do {
        book: { 
          author: 'Someone'
        }
      }
    end

    it 'updates books information' do
      login
      edit_book

      expect(Book.last.author).to eq('Someone')
    end
  end

  describe 'DELETE #destroy' do
    before do
      create(:book)
    end

    subject(:delete_book) do
      delete book_url(Book.last.id)
    end

    it 'removes a new book' do
      login

      expect{delete_book}.to change{Book.count}.by(-1)
    end
  end
end