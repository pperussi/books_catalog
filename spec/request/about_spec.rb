# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/about', type: :request do
  describe 'GET #index' do
    it 'requests information about the project' do
      get about_index_url('/about')

      expect(response.body).to include('Projeto Catálogo de Livros')
    end
  end
end