# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Book, type: :model do
  describe 'attributes' do
    it { is_expected.to respond_to :title }
    it { is_expected.to respond_to :description }
    it { is_expected.to respond_to :category }
    it { is_expected.to respond_to :pages }
    it { is_expected.to respond_to :author }
    it { is_expected.to respond_to :image_url }
  end

  describe 'title' do
    it 'do not create with title null' do
      expect do
        create(:book, title: nil)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end

    it 'do not create twice the same title' do
      create(:book, title: 'Livro')

      expect do
        create(:book, title: 'Livro')
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

  describe 'description' do
    it 'do not create with title null' do
      expect do
        create(:book, description: nil)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end
  end

  describe 'category' do
    it 'do not create with title null' do
      expect do
        create(:book, category: nil)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end
  end

  describe 'pages' do
    it 'do not create with title null' do
      expect do
        create(:book, pages: nil)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end
  end

  describe 'author' do
    it 'do not create with title null' do
      expect do
        create(:book, author: nil)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end
  end

  describe 'image_url' do
    it 'do not create with image_url null' do
      book = create(:book)
        
      expect(book.image_url).to eq(ActionController::Base.helpers.asset_path("default_book_image.jpeg"))
    end

    it 'do not create with empty image_url' do
      book = create(:book, image_url: "")
        
      expect(book.image_url).to eq(ActionController::Base.helpers.asset_path("default_book_image.jpeg"))
    end
  end

  describe 'scopes' do
    it 'searchs by term' do
      5.times do
        create(:book)
      end

      right_book_one = create(:book, title: "Alice in Wonderland")
      right_book_two = create(
        :book,
        description: "Alice again enters a fantastical world, this time by climbing through" \
          " a mirror into the world that she can see beyond it."
      )
      books = Book.all

      expect(books.term_search("alice")).to eq([right_book_one, right_book_two])
    end

    it 'searchs by author' do
      5.times do
        create(:book)
      end

      right_book = create(:book, author: "Neil Gaiman")
      books = Book.all

      expect(books.term_search("gaiman")).to eq([right_book])
    end
  end
end
