# frozen_string_literal: true

class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:create, :update, :destroy]

  def index
    @books = filter(params[:query])

    sort(@books, params[:order])

    respond_to do |format|
      format.html
      format.json { render json: { books: @books } }
    end
  end
 
  def show
  end

  def edit
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)

    if @book.save
      redirect_to @book, notice: 'Livro criado com sucesso.'
    else
      render :new
    end
  end

  def update
    if @book.update(book_params)
      redirect_to @book, notice: 'Livro atualizado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to books_url, notice: 'Livro removido com sucesso.'
  end

  private

    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:title, :description, :pages, :category, :author, :image_url)
    end

    def filter(query)
      return Book.term_search(query) if query.present?
        
      Book.all
    end

    def sort(books, order)
      return if order.nil? || order == ""

      @books = books.order("title #{order}")
    end
end