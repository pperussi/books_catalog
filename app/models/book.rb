class Book < ApplicationRecord
  include PgSearch::Model

  before_save :default_values

  pg_search_scope :term_search,
    against: [:title, :description, :author],
    using: {
      tsearch: { prefix: true }
  }

  def default_values
    if self.image_url.nil? || self.image_url == ""
      self.image_url = ActionController::Base.helpers.asset_path("default_book_image.jpeg")
    end
  end
end
