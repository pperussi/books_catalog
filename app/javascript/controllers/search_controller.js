import { Controller } from 'stimulus'; export default class extends Controller {
  static targets = ["query", "books", "order"]

  // submit() {
  //   const value = this.queryTarget.value
  //   fetch(`/?query=${value}`, {
  //     headers: { accept: 'application/json'}
  //   }).then((response) => response.json())
  //   .then(data => {
  //     var bookHTML = "";
  //    var bookArray = Object.values(data)[0]
  //    bookArray.forEach(book => {
  //     bookHTML += this.bookTemplate(book)
  //   });
  //    this.booksTarget.innerHTML = bookHTML;
  //  });
  // }

  submit() {
    const value = this.queryTarget.value
    const order = event.target.type == "radio" ? event.target.value : ""

    fetch(`/?query=${value}&order=${order}`, {
      headers: { accept: 'application/json' }
    }).then((response) => response.json())
      .then(data => {
        var bookHTML = "";
        var bookArray = Object.values(data)[0]
        bookArray.forEach(book => {
          bookHTML += this.bookTemplate(book)
        });
        this.booksTarget.innerHTML = bookHTML;
      });
  }

  bookTemplate(book) {
    return `
    <div class="card shadow-sm m-2 col-md-2">
      <svg class="bd-placeholder-img card-img-top" width="100%" height="20"
        xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder:
        Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">
      </svg><img src="${book.image_url}"></img>
      <div class="card-body">
        <p class="card-text">${book.title}
          <br>
          <small class="text-muted">${book.author}</small>
        </p>
        <div class="d-flex justify-content-between align-items-right">
          <div class="btn-group">
          <button type="button" class="btn btn-sm btn-outline-secondary"><a
                    style="color: gray" href="/books/${book.id}">Detalhes</a></button>
          </div>
        </div>
      </div>
    </div>`
  }
}